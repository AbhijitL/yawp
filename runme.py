from YAWeP import Weather
# Import the library YAWep (Yet another weather package api)
# install on your system via pip install YAWeP

help(Weather) # this will printout all the methods for the class

#Let's get the weather info for jaipur
jaiW = Weather("26631f0f41b95fb9f5ac0df9a8f43c92","Jaipur")

#Let's get the current weather of jaipur raw
print(jaiW.current_h())
#Let's get the current weather simplified 
print(jaiW.current_h_simplified())

#Let's get the weather report for Jaipur for  next 12 hours raw
print(jaiW.next_12h())
#Let's get the weather for Jaipur for next 12 hours simplified 
print(jaiW.next_12h_simplified())

# can also get solar radiation data for current lattitude and longitude lets say Jaipur
# this will work if you have the api ascess
"""
jaiWR = Weather("26631f0f41b95fb9f5ac0df9a8f43c92","26.9124","75.7873")
print(jaiWR.current_solar_radiation())
"""
